package com.sparkslab.letsmovie;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;

public class MainActivity extends Activity {
	private Button button_login;
	private ProgressDialog pDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViews();
		setUpViews();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onPause() {
		if (pDialog != null) {
			pDialog.dismiss();
		}
		super.onPause();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);
	}

	private void findViews() {
		button_login = (Button) findViewById(R.id.button_login);
	}

	private void setUpViews() {
		button_login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				doLogin();
			}
		});
	}

	protected void doLogin() {
		pDialog = ProgressDialog.show(this, "Please wait", "Logging in...",
				true, true, new DialogInterface.OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {
						Toast.makeText(getApplicationContext(),
								"Login cancelled.", Toast.LENGTH_LONG).show();
					}
				});

		Session.openActiveSession(this, true, new Session.StatusCallback() {

			@Override
			public void call(final Session session, SessionState state,
					Exception exception) {
				if (session.isOpened()) {
					Request.executeMeRequestAsync(session,
							new Request.GraphUserCallback() {

								@Override
								public void onCompleted(GraphUser user,
										Response response) {
									pDialog.dismiss();
									if (user != null) {
										
										intentToHall(user.getId(),
												user.getName());
									} else {
										Toast.makeText(
												MainActivity.this,
												"Unable to login, check your Internet settings.",
												Toast.LENGTH_LONG).show();
									}
								}
							});
				}
			}
		});
	}

	private void intentToHall(String user_id, String user_name) {
		Intent intent_update = new Intent(this, HallActivity.class);
		Bundle bundle = new Bundle();
		bundle.putString("user_id", user_id);
		bundle.putString("user_name", user_name);
		intent_update.putExtras(bundle);
		startActivity(intent_update);
		finish();
	}
}
