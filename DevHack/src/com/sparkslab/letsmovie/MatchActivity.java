package com.sparkslab.letsmovie;

import java.net.URL;

import com.sparkslab.lets.callbacks.APICallBack;
import com.sparkslab.letsmovie.helper.MovieHelper;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MatchActivity extends Activity {

	private String match_id;
	private TextView match_name;
	private Point screen_dimen;

	private ImageView imageView_match;
	private TextView textView_match_name;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_match);
		screen_dimen = getScreenSize();
		getBundle();
		getViews();
		setUpViews();
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	@SuppressWarnings("deprecation")
	private Point getScreenSize() {
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			Point size = new Point();
			display.getSize(size);
			return size;
		} else {
			Point size = new Point();
			size.x = display.getWidth();
			size.y = display.getHeight();
			return size;
		}
	}

	private void getBundle() {
		Bundle bundle = getIntent().getExtras();
		match_id = bundle.getString("match_id");
	}

	private void getViews() {
		imageView_match = (ImageView) findViewById(R.id.imageView_match);
		textView_match_name = (TextView) findViewById(R.id.textView_match_name);
	}

	private void setUpViews() {
		new SetUpMatchPhotoTask().execute();
		MovieHelper.getFacebookName(match_id, new APICallBack() {

			@Override
			public void onSuccess(String result) {
				textView_match_name.setText(result);
				super.onSuccess(result);
			}

			@Override
			public void onFailure(String errorMessage) {
				super.onFailure(errorMessage);
			}

		});
		imageView_match.setLayoutParams(new RelativeLayout.LayoutParams(
				screen_dimen.x, screen_dimen.x));
		imageView_match.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri
						.parse("fb://profile/" + match_id)));
			}
		});
	}

	class SetUpMatchPhotoTask extends AsyncTask<Void, Void, Void> {

		private Bitmap match_photo;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				URL img_url = new URL("http://graph.facebook.com/" + match_id
						+ "/picture?width=96&height=128");
				match_photo = BitmapFactory.decodeStream(img_url
						.openConnection().getInputStream());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			imageView_match.setImageBitmap(match_photo);
			new SetUpHiResPhotoTask().execute();
		}
	}

	class SetUpHiResPhotoTask extends AsyncTask<Void, Void, Void> {

		private Bitmap match_photo;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				URL img_url = new URL("http://graph.facebook.com/" + match_id
						+ "/picture?width=384&height=512");
				match_photo = BitmapFactory.decodeStream(img_url
						.openConnection().getInputStream());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			imageView_match.setImageBitmap(match_photo);
		}
	}
}
