package com.sparkslab.letsmovie.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sparkslab.lets.callbacks.APICallBack;
import com.sparkslab.lets.callbacks.MovieCallback;
import com.sparkslab.lets.models.CheckModel;
import com.sparkslab.lets.models.MovieModel;
import com.sparkslab.lets.models.MovieModel.Theater;
import com.sparkslab.lets.models.MovieTableModel;
import com.sparkslab.letsmovie.Constant;
import com.sparkslab.letsmovie.libs.Utils;

public class MovieHelper {
	private static AsyncHttpClient mClient = initClient();

	public static AsyncHttpClient initClient() {
		AsyncHttpClient client = new AsyncHttpClient();
		client.addHeader("Connection", "Keep-Alive");
		client.setTimeout(30 * 1000);

		return client;
	}

	public static List<MovieModel> getMovieList(final MovieCallback callback) {
		final List<MovieModel> movies = new ArrayList<MovieModel>();

		String url = "http://1.34.40.115/movies.json";

		mClient.get(url, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, JSONArray response) {
				super.onSuccess(statusCode, response);

				if (statusCode == 200) {
					if (response != null) {
						for (int i = 0; i < response.length(); i++) {
							MovieModel model = new MovieModel();

							try {
								JSONObject movieObject = response
										.getJSONObject(i);

								model.chinese_movie_name = movieObject
										.getString("chinese_movie_name");
								model.english_movie_name = movieObject
										.getString("english_movie_name");
								model.movie_subtitle = movieObject
										.getString("movie_subtitle");
								model.movie_poster_url = movieObject
										.getString("movie_poster_url");
								model.movie_language = movieObject
										.getString("movie_language");
								model.movie_description = movieObject
										.getString("movie_description");
								model.movie_genre = movieObject
										.getString("movie_genre");
								model.movie_actors = movieObject
										.getString("movie_actors");
								model.movie_length = movieObject
										.getString("movie_length");
								model.movie_rating = movieObject
										.getString("movie_rating");

								model.theaters = new ArrayList<MovieModel.Theater>();
								JSONArray theaterArray = movieObject
										.getJSONArray("theater");

								if (theaterArray != null) {
									for (int j = 0; j < theaterArray.length(); j++) {
										JSONObject theaterObject = theaterArray
												.getJSONObject(j);

										Theater theater = new Theater();
										theater.theater_name = theaterObject
												.getString("theater_name");
										theater.theater_address = theaterObject
												.getString("theater_address");
										theater.theater_latitude = theaterObject
												.getDouble("theater_latitude");
										theater.theater_longitude = theaterObject
												.getDouble("theater_longitude");
										theater.theater_times = new ArrayList<String>();

										JSONArray timeArray = theaterObject
												.getJSONArray("theater_times");
										if (timeArray != null) {
											for (int k = 0; k < timeArray
													.length(); k++) {
												theater.theater_times
														.add(timeArray
																.getString(k));
											}
										}
										model.theaters.add(theater);
									}
								}

							} catch (JSONException e) {
								e.printStackTrace();
								callback.onFailure(e.toString());
							}

							movies.add(model);
						}

						if (callback != null) {
							callback.onSuccess(movies);
						}
					} else {
						if (callback != null) {
							callback.onFailure("response is null");
						}
					}
				} else {
					if (callback != null) {
						callback.onFailure("status_code: " + statusCode);
					}
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);

				if (callback != null) {
					callback.onFailure(content);
				}
			}
		});

		return movies;
	}

	public static void setLogin(String fbname, String fbUID, double longtitude,
			double latitude, final APICallBack callback) {
		RequestParams params = new RequestParams();
		params.put("name", fbname);
		params.put("uid", fbUID);
		params.put("longtitude", longtitude + "");
		params.put("latitude", latitude + "");

		mClient.post(Utils.URL_BASE_API + "/api/register", params,
				new JsonHttpResponseHandler() {
					@Override
					public void onSuccess(int statusCode, Header[] headers,
							JSONObject response) {
						try {
							callback.onSuccess(response.getString("status"));
							// successful or duplicate
						} catch (JSONException e) {
							callback.onFailure(e.toString());
						}
						super.onSuccess(statusCode, headers, response);
					}

					@Override
					public void onFailure(Throwable error, String content) {
						super.onFailure(error, content);
						callback.onFailure(content);
					}

				});
	}

	public static void setPreferTime(String fbUid, int hour, int weekday,
			double longtitude, double latitude, final APICallBack callback) {
		RequestParams params = new RequestParams();
		params.put("hour", String.valueOf(hour));
		params.put("weekday", String.valueOf(weekday));
		params.put("longtitude", String.valueOf(longtitude));
		params.put("latitude", String.valueOf(latitude));
		params.put("uid", fbUid);
		mClient.post(Utils.URL_BASE_API + "/api/set_prefer_time", params,
				new JsonHttpResponseHandler() {
					@Override
					public void onSuccess(int statusCode, Header[] headers,
							JSONObject response) {
						try {
							callback.onSuccess(response.getString("status"));
							// successful or user not in database
						} catch (JSONException e) {
							callback.onFailure(e.toString());
						}
						super.onSuccess(statusCode, headers, response);
					}

					@Override
					public void onFailure(Throwable error, String content) {
						super.onFailure(error, content);
						callback.onFailure(content);
					}

				});
	}

	public static void getBuddy(String fbUID, final APICallBack callback) {
		RequestParams params = new RequestParams();
		params.put("uid", fbUID);
		mClient.post(Utils.URL_BASE_API + "/api/get_buddy", params,
				new JsonHttpResponseHandler() {
					@Override
					public void onSuccess(int statusCode, Header[] headers,
							JSONObject response) {
						super.onSuccess(statusCode, headers, response);
						try {
							callback.onSuccess(response.getString("status"));
							Log.e(Constant.TAG,"asdad");
						} catch (JSONException e) {
							callback.onFailure(e.toString());
						}
					}

					@Override
					public void onFailure(Throwable error, String content) {
						super.onFailure(error, content);
						callback.onFailure(content);
					}

				});
	}

	public static void getWeekTable(String fbUID, final APICallBack callback) {
		RequestParams params = new RequestParams();
		params.put("uid", fbUID);
		mClient.post(Utils.URL_BASE_API + "/api/get_people", params,
				new JsonHttpResponseHandler() {

					@Override
					public void onSuccess(int statusCode, Header[] headers,
							JSONObject response) {
						try {
							MovieTableModel model = new MovieTableModel();

							JSONArray peopleArray = response
									.getJSONArray("people");
							if (peopleArray != null) {
								int[] people = new int[peopleArray.length()];
								for (int i = 0; i < peopleArray.length(); i++) {
									people[i] = peopleArray.getInt(i);
								}
								model.setPeople(people);
							}

							JSONArray densityArray = response
									.getJSONArray("density");
							if (densityArray != null) {
								int[] density = new int[densityArray.length()];
								for (int i = 0; i < peopleArray.length(); i++) {
									density[i] = densityArray.getInt(i);
								}
								model.setDesity(density);
							}

							callback.onSuccess(model);

							// successful or user not in database
						} catch (JSONException e) {
							callback.onFailure(e.toString());
						}
						super.onSuccess(statusCode, headers, response);
					}

					@Override
					public void onFailure(Throwable error, String content) {
						super.onFailure(error, content);
						callback.onFailure(content);
					}

				});
	}

	public static void getCheck(String uid, final APICallBack callback) {
		RequestParams params = new RequestParams();
		params.put("uid", uid);

		mClient.post(Utils.URL_BASE_API + "/api/check", params,
				new JsonHttpResponseHandler() {

					@Override
					public void onSuccess(int statusCode, JSONObject response) {
						super.onSuccess(statusCode, response);

						CheckModel model = new CheckModel();

						try {
							model.status = response.getString("status");
							if (response.has("user")) {
								JSONObject userObject = response
										.getJSONObject("user");
								model.user_count = userObject.getInt("count");
								model.user_name = userObject.getString("name");
								model.user_score = userObject.getInt("score");
								model.user_count = userObject.getInt("count");
								model.user_timestamp = userObject
										.getString("timestamp");
								model.user_uid = userObject.getString("uid");
							}
							callback.onSuccess(model);
						} catch (JSONException e) {
							e.printStackTrace();
							callback.onFailure(e.toString());
						}
					}

					@Override
					protected void sendFailureMessage(Throwable e,
							String responseBody) {
						super.sendFailureMessage(e, responseBody);

						callback.onFailure(responseBody);
					}
				});
	}

	public static void getFacebookName(String user_id,
			final APICallBack callback) {
		mClient.get("http://graph.facebook.com/" + user_id,
				new JsonHttpResponseHandler() {

					@Override
					public void onSuccess(int statusCode, JSONObject response) {
						super.onSuccess(statusCode, response);

						if (statusCode == 200) {
							try {
								callback.onSuccess(response.getString("name"));
							} catch (JSONException e) {
								e.printStackTrace();
								callback.onFailure(e.toString());
							}
						} else {
							callback.onFailure("The status code is: "
									+ statusCode);
						}
					}

					@Override
					public void onFailure(Throwable error, String content) {
						super.onFailure(error, content);

						callback.onFailure(content);
					}
				});
	}
}
