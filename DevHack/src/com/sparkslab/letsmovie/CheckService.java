package com.sparkslab.letsmovie;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;

import com.sparkslab.lets.callbacks.APICallBack;
import com.sparkslab.lets.models.CheckModel;
import com.sparkslab.letsmovie.helper.MovieHelper;

public class CheckService extends Service {
	private String user_id;
	private NotificationCompat.Builder builder;
	private NotificationManager manager;
	public static final int NOTIFICATION_ID = 666;
	private static final int WHAT_CHECK = 404;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		user_id = intent.getStringExtra("uid");

		Message m = handler.obtainMessage();
		m.arg1 = Constant.STATUS_START;
		m.arg2 = WHAT_CHECK;
		handler.sendMessage(m);

		return super.onStartCommand(intent, flags, startId);
	}

	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);

			switch (msg.arg1) {
			case Constant.STATUS_START:
				new Thread(runnableCheck).start();
				break;
			case Constant.STATUS_SUCCESS:
				addNotification();
				stopSelf();
				break;
			case Constant.STATUS_ERROR:
				stopSelf();
				break;
			}
		}
	};

	private Runnable runnableCheck = new Runnable() {

		@Override
		public void run() {
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			MovieHelper.getCheck(user_id, new APICallBack() {

				@Override
				public void onSuccess(CheckModel result) {
					super.onSuccess(result);

					if (result.status.equals("paired")) {
						Message m = handler.obtainMessage();
						m.arg1 = Constant.STATUS_SUCCESS;
						m.arg2 = WHAT_CHECK;
						handler.sendMessage(m);
					} else {
						Message m = handler.obtainMessage();
						m.arg1 = Constant.STATUS_START;
						m.arg2 = WHAT_CHECK;
						handler.sendMessage(m);
					}
				}

				@Override
				public void onFailure(String errorMessage) {
					super.onFailure(errorMessage);

					Message m = handler.obtainMessage();
					m.arg1 = Constant.STATUS_START;
					m.arg2 = WHAT_CHECK;
					handler.sendMessage(m);
				}
			});
			
			MovieHelper.getBuddy(user_id,new APICallBack(){
				@Override
				public void onSuccess(String result) {
					
					super.onSuccess(result);
				}
			});
		}
	};

	private void addNotification() {
		builder = new NotificationCompat.Builder(getApplicationContext())
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle("Let's Movie").setTicker("Match found!")
				.setContentText("Match found!");

		Intent notificationIntent = new Intent(getApplicationContext(),
				HallActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, 0);
		builder.setContentIntent(contentIntent);

		// Add as notification
		manager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);
		manager.notify(NOTIFICATION_ID, builder.build());
	}
}
