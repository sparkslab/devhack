package com.sparkslab.letsmovie;

public class Constant {
	public static final String TAG = "SparksLab";
	public static final int STATUS_START = 100;
	public static final int STATUS_SUCCESS = 101;
	public static final int STATUS_ERROR = 102;

}
