package com.sparkslab.letsmovie.libs;

import android.content.Context;
import android.graphics.Typeface;

public class Utils {
	public static String URL_BASE_API = "http://192.168.43.224:5000";

	public static int getDpPixels(Context context, int dps) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dps * scale + 0.5f);
	}
	
	public static Typeface getTypeface(Context context, String name) {
		return Typeface.createFromAsset(context.getAssets(), "typeface/" + name
				+ ".ttf");
	}
}
