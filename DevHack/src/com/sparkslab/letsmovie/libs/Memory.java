package com.sparkslab.letsmovie.libs;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Memory {
	private static final String APP_SHARED_PREFS = "com.sparkslab.letsmovie";
	private SharedPreferences appSharedPrefs;
	private Editor prefsEditor;

	public Memory(Context context) {
		this.appSharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS,
				Activity.MODE_PRIVATE);
		this.prefsEditor = appSharedPrefs.edit();
	}

	public int getInt(String key) {
		return appSharedPrefs.getInt(key, 0);
	}

	public void setInt(String key, int value) {
		prefsEditor.putInt(key, value);
		prefsEditor.commit();
	}

	public String getString(String key) {
		return appSharedPrefs.getString(key, "");
	}

	public void setString(String key, String data) {
		prefsEditor.putString(key, data);
		prefsEditor.commit();
	}
}