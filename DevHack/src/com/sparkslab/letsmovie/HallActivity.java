package com.sparkslab.letsmovie;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Request.Callback;
import com.facebook.Response;
import com.facebook.Session;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.loopj.android.image.SmartImageView;
import com.sparkslab.lets.callbacks.APICallBack;
import com.sparkslab.lets.callbacks.MovieCallback;
import com.sparkslab.lets.models.CheckModel;
import com.sparkslab.lets.models.MovieModel;
import com.sparkslab.lets.models.MovieModel.Theater;
import com.sparkslab.lets.models.MovieTableModel;
import com.sparkslab.letsmovie.helper.MovieHelper;
import com.sparkslab.letsmovie.libs.Memory;
import com.sparkslab.letsmovie.libs.Utils;

public class HallActivity extends FragmentActivity implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	private String user_id, user_name, match_id;
	private int[] table_color = { 0xffe2f4fb, 0xffc5eaf8, 0xffa8dff4,
			0xff8ad5f0, 0xff6dcaec, 0xff50c0e9, 0xff33b5e5, 0xff2cb1e1,
			0xff16a5d7, 0xff0099cc, 0xffcc0000, 0xff669900 };
	private int[] table_density_data;
	public int[] table_people_data;
	private int[] hour = { 0, 3, 6, 9, 12, 15, 18, 21 };
	private ImageView imageView_user, imageView_match;
	private TextView textView_user_name, textView_chosen;
	private TableLayout table;
	private HorizontalScrollView horizontalScrollView_movies,
			horizontalScrollView_friends;
	private LinearLayout layout_movies, layout_friends;
	private List<String> friend_list;

	private double longtitude, latitude;
	private CountDownTimer autoScrollTimer, autoScrollTimerFriends;
	private Calendar today;
	private Point screen_dimen;
	private LocationClient mLocationClient;
	private List<MovieModel> movies;

	private final static int WHAT_GET_MOVIES = 404;
	private final static int WHAT_GET_FRIENDS = 405;
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hall);

		initValues();
		getBundle();
		getViews();
		setUpViews();
		getMovieDatas();
		getFriends();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.hall, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_refresh:
			refresh();
			break;

		default:
			break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	@SuppressWarnings("deprecation")
	private Point getScreenSize() {
		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			Point size = new Point();
			display.getSize(size);
			return size;
		} else {
			Point size = new Point();
			size.x = display.getWidth();
			size.y = display.getHeight();
			return size;
		}
	}

	private void initValues() {
		friend_list = new ArrayList<String>();

		screen_dimen = getScreenSize();
		today = Calendar.getInstance();
		mLocationClient = new LocationClient(this, this, this);

	}

	private void getBundle() {
		Memory memory = new Memory(this);
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			user_id = bundle.getString("user_id");
			user_name = bundle.getString("user_name");
			memory.setString("user_id", user_id);
			memory.setString("user_name", user_name);
		} else {
			user_id = memory.getString("user_id");
			user_name = memory.getString("user_name");
		}
	}

	private void getViews() {
		imageView_user = (ImageView) findViewById(R.id.imageView_user);
		textView_user_name = (TextView) findViewById(R.id.textView_user_name);
		table = (TableLayout) findViewById(R.id.table);
		layout_movies = (LinearLayout) findViewById(R.id.layout_movies);
		layout_friends = (LinearLayout) findViewById(R.id.layout_friends);
		horizontalScrollView_movies = (HorizontalScrollView) findViewById(R.id.horizontalScrollView_movies);
		horizontalScrollView_friends = (HorizontalScrollView) findViewById(R.id.horizontalScrollView_friends);
		textView_chosen = (TextView) findViewById(R.id.textView_chosen);
	}

	private void setUpViews() {
		new SetUpIdPhotoTask().execute();
		textView_user_name.setText(user_name);
		setUpTypefaces();
		setUpMovies();
	}

	private void setUpTypefaces() {
		((TextView) findViewById(R.id.textView_movies)).setTypeface(Utils
				.getTypeface(this, "Roboto-Light"));
		((TextView) findViewById(R.id.textView_week_table)).setTypeface(Utils
				.getTypeface(this, "Roboto-Light"));
		((TextView) findViewById(R.id.textView_friends)).setTypeface(Utils
				.getTypeface(this, "Roboto-Light"));
	}

	private void setUpMovies() {
		LinearLayout.LayoutParams itemParams = new LinearLayout.LayoutParams(
				Utils.getDpPixels(HallActivity.this, 180), Utils.getDpPixels(
						HallActivity.this, 240));

		for (int i = 0; i <= 32; i++) {
			ImageView imageView = new ImageView(HallActivity.this);
			try {
				imageView.setLayoutParams(itemParams);
				imageView.setScaleType(ScaleType.CENTER_CROP);
				imageView.setImageDrawable(Drawable.createFromStream(
						getAssets().open("poster_" + i + ".jpg"), null));
				if (i != 32) {
					imageView.setPadding(0, 0, Utils.getDpPixels(this, 8), 0);
				}
				layout_movies.addView(imageView);
				final int index = i;
				imageView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						goToMovie(index);
					}
				});
				imageView.setOnTouchListener(new OnTouchListener() {

					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if (autoScrollTimer != null) {
							autoScrollTimer.cancel();
						}
						return false;
					}
				});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		autoScrollTimer = new CountDownTimer(6400000, 10) {

			@Override
			public void onTick(long millisUntilFinished) {
				horizontalScrollView_movies.scrollBy(1, 0);
			}

			@Override
			public void onFinish() {
			}
		};
		autoScrollTimer.start();
	}

	private void setupFriends() {
		LinearLayout.LayoutParams itemParams = new LinearLayout.LayoutParams(
				Utils.getDpPixels(HallActivity.this, 150), Utils.getDpPixels(
						HallActivity.this, 150));

		for (final String friend : friend_list) {
			SmartImageView imageView = new SmartImageView(HallActivity.this);

			imageView.setLayoutParams(itemParams);
			imageView.setScaleType(ScaleType.CENTER_CROP);
			imageView.setImageUrl("http://graph.facebook.com/" + friend
					+ "/picture?width=150&height=150");
			imageView.setPadding(0, 0, Utils.getDpPixels(this, 8), 0);
			layout_friends.addView(imageView);
			imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						getPackageManager().getPackageInfo(
								"com.facebook.katana", 0);
						startActivity(new Intent(Intent.ACTION_VIEW, Uri
								.parse("fb://profile/" + friend)));
					} catch (Exception e) {
						startActivity(new Intent(
								Intent.ACTION_VIEW,
								Uri.parse("https://www.facebook.com/<user_name_here>")));
					}
				}
			});

			imageView.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (autoScrollTimerFriends != null) {
						autoScrollTimerFriends.cancel();
					}
					return false;
				}
			});
		}
		autoScrollTimerFriends = new CountDownTimer(6400000, 10) {

			@Override
			public void onTick(long millisUntilFinished) {
				horizontalScrollView_friends.scrollBy(1, 0);
			}

			@Override
			public void onFinish() {
			}
		};
		autoScrollTimerFriends.start();
	}

	private void setUpTable() {
		int rows = 8, cols = 9;
		Calendar calendar_display = Calendar.getInstance();
		TableRow.LayoutParams rowParams = new TableRow.LayoutParams(
				TableRow.LayoutParams.MATCH_PARENT,
				TableRow.LayoutParams.WRAP_CONTENT);
		final int cell_width = (int) Math.ceil((screen_dimen.x
				- Utils.getDpPixels(this, 16) - cols)
				/ (double) cols);
		TableRow.LayoutParams cellParams = new TableRow.LayoutParams(
				cell_width, cell_width);
		cellParams.setMargins(1, 1, 0, 0);
		for (int row = 0; row < rows; row++) {
			TableRow tableRow = new TableRow(this);
			tableRow.setLayoutParams(rowParams);
			for (int col = 0; col < cols; col++) {
				View cell;
				if (row == 0 || col == 0) {
					cell = new TextView(this);
					((TextView) cell).setTextColor(0xffffffff);
					((TextView) cell).setGravity(Gravity.CENTER);
					cell.setBackgroundColor(0xff555555);
					if (row == 0 && col != 0) {
						((TextView) cell)
								.setText(String.valueOf((col - 1) * 3));
					} else if (col == 0 && row != 0) {
						((TextView) cell).setText(calendar_display
								.getDisplayName(Calendar.DAY_OF_WEEK,
										Calendar.SHORT, Locale.US));
					}
				} else {
					if (row == 1
							&& (col - 1) * 3 <= today.get(Calendar.HOUR_OF_DAY)) {
						cell = new TextView(this);
						((TextView) cell).setGravity(Gravity.CENTER);
						((TextView) cell).setText("X");
					} else {
						cell = new ImageView(this);
					}
					int population = table_density_data[(row - 1) * (cols - 1)
							+ (col - 1)];
					cell.setBackgroundColor(table_color[population]);
					if (population == 10) {
						String friendlyDateDisplay;
						if (row == 1) {
							friendlyDateDisplay = "today on " + ((col - 1) * 3)
									+ ":00.";
						} else if (row == 2) {
							friendlyDateDisplay = "tomorrow on "
									+ ((col - 1) * 3) + ":00.";
						} else {
							friendlyDateDisplay = "on "
									+ calendar_display.getDisplayName(
											Calendar.MONTH, Calendar.SHORT,
											Locale.US) + " "
									+ calendar_display.get(Calendar.DATE)
									+ ", " + ((col - 1) * 3) + ":00.";
						}
						imageView_match = (ImageView) cell;
						textView_chosen.setText("You have a pending request "
								+ friendlyDateDisplay);
						textView_chosen.setTextColor(table_color[10]);
						match_id = user_id;
						new SetUpMatchPhotoTask().execute();
					} else if (population == 11) {
						cell.setBackgroundColor(table_color[population]);
						imageView_match = (ImageView) cell;
						textView_chosen.setTextColor(table_color[11]);
						textView_chosen
								.setText("You just matched somebody!\nPlease wait for the result...");
						MovieHelper.getCheck(user_id, new APICallBack() {

							@Override
							public void onSuccess(CheckModel result) {
								match_id = result.user_uid;
								new SetUpMatchPhotoTask().execute();
								textView_chosen
										.setText("You just matched somebody!\nTap here to see the result.");
								textView_chosen
										.setOnClickListener(new OnClickListener() {

											@Override
											public void onClick(View v) {
												Intent intent_match = new Intent(
														HallActivity.this,
														MatchActivity.class);
												Bundle bundle = new Bundle();
												bundle.putString("match_id",
														match_id);
												intent_match.putExtras(bundle);
												startActivity(intent_match);
											}
										});
								super.onSuccess(result);
							}

							@Override
							public void onFailure(String errorMessage) {
								super.onFailure(errorMessage);
							}
						});
					}
					final int r = row;
					final int c = col;
					final int color = table_color[population];
					cell.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							int selected_hour = hour[c - 1];
							int selected_weekday = r - 1;
							// Log.e(Constant.TAG, "hour = " + selected_hour
							// + " weekday = " + selected_weekday);
							goToDetail(selected_hour, selected_weekday, color);
						}
					});
				}
				cell.setLayoutParams(cellParams);
				tableRow.addView(cell);
			}
			table.addView(tableRow);
			if (row != 0) {
				calendar_display.add(Calendar.DATE, 1);
			}
		}
	}

	private void goToDetail(final int selected_hour, final int selected_weekday, final int color) {
		
		Intent intent_update = new Intent(HallActivity.this,
				SetTimeActivity.class);
		Bundle bundle = new Bundle();
		bundle.putString("uid", user_id);
		bundle.putInt("hour", selected_hour);
		bundle.putInt("weekday", selected_weekday);
		bundle.putDouble("longtitude", longtitude);
		bundle.putDouble("latitude", latitude);
		bundle.putInt("color", color);
		intent_update.putExtras(bundle);
		startActivity(intent_update);
		
		
	}

	private void goToMovie(int index) {
		Intent intent_update = new Intent(HallActivity.this,
				MovieActivity.class);
		Bundle bundle = new Bundle();
		try {
			MovieModel model = movies.get(index);

			bundle.putInt("index", index);
			bundle.putString("chinese_movie_name", model.chinese_movie_name);
			bundle.putString("english_movie_name", model.english_movie_name);
			bundle.putString("movie_subtitle", model.movie_subtitle);
			bundle.putString("movie_language", model.movie_language);
			bundle.putString("movie_description", model.movie_description);
			bundle.putString("movie_genre", model.movie_genre);
			bundle.putString("movie_actors", model.movie_actors);
			bundle.putString("movie_length", model.movie_length);
			bundle.putString("movie_poster_url", model.movie_poster_url);

			StringBuilder sb = new StringBuilder();
			if (model.theaters != null) {
				for (Theater theater : model.theaters) {
					sb.append(theater.theater_name + "\n");
					sb.append(theater.theater_address + "\n");
					for (String time : theater.theater_times) {
						sb.append(time + "  ");
					}
					sb.append("\n\n");
				}
				bundle.putString("theaters", sb.toString());
			} else {
				bundle.putString("theaters", "");
			}
			intent_update.putExtras(bundle);
			startActivity(intent_update);
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
		}
	}

	private void getMovieDatas() {
		Message m = handler.obtainMessage();
		m.arg1 = Constant.STATUS_START;
		m.arg2 = WHAT_GET_MOVIES;
		handler.sendMessage(m);
	}

	private void getFriends() {
		Message m = handler.obtainMessage();
		m.arg1 = Constant.STATUS_START;
		m.arg2 = WHAT_GET_FRIENDS;
		handler.sendMessage(m);
	}

	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);

			switch (msg.arg1) {
			case Constant.STATUS_START:
				if (msg.arg2 == WHAT_GET_MOVIES) {
					new Thread(runnableGetMovies).start();
				} else if (msg.arg2 == WHAT_GET_FRIENDS) {
					new Thread(runnableGetInstalledFriends).start();
				}
				break;
			case Constant.STATUS_SUCCESS:
				if (msg.arg2 == WHAT_GET_MOVIES) {
				} else if (msg.arg2 == WHAT_GET_FRIENDS) {
					setupFriends();
				}
				break;
			case Constant.STATUS_ERROR:
				if (msg.arg2 == WHAT_GET_MOVIES) {

				} else if (msg.arg2 == WHAT_GET_FRIENDS) {

				}
				break;
			}
		}
	};

	private Runnable runnableGetMovies = new Runnable() {

		@Override
		public void run() {
			MovieHelper.getMovieList(new MovieCallback() {

				@Override
				public void onSuccess(List<MovieModel> movies) {
					super.onSuccess(movies);
					HallActivity.this.movies = movies;
				}

				@Override
				public void onFailure(String errorMessage) {
					super.onFailure(errorMessage);
				}
			});
		}
	};

	private Runnable runnableGetInstalledFriends = new Runnable() {
		private Message m;

		@Override
		public void run() {
			m = handler.obtainMessage();
			m.arg2 = WHAT_GET_FRIENDS;

			friend_list.clear();

			Bundle bundle = new Bundle();
			bundle.putString("fields", "installed,id");

			Request request = new Request(Session.getActiveSession(),
					"me/friends", bundle, HttpMethod.GET, new Callback() {

						@Override
						public void onCompleted(Response response) {
							try {
								JSONArray dataArray = response.getGraphObject()
										.getInnerJSONObject()
										.getJSONArray("data");
								for (int i = 0; i < dataArray.length(); i++) {
									JSONObject dataObject = dataArray
											.getJSONObject(i);
									boolean isInstalled = false;
									if (dataObject.has("installed")) {
										isInstalled = dataObject
												.getBoolean("installed");
									}

									if (isInstalled) {
										String id = dataObject.getString("id");
										friend_list.add(id);
									}
								}

								m.arg1 = Constant.STATUS_SUCCESS;
								handler.sendMessage(m);
							} catch (JSONException e) {
								e.printStackTrace();

								m.arg1 = Constant.STATUS_ERROR;
								m.obj = e.toString();
								handler.sendMessage(m);
							}
						}
					});

			request.executeAndWait();
		}
	};

	class SetUpIdPhotoTask extends AsyncTask<Void, Void, Void> {

		private Bitmap user_photo;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				URL img_url = new URL("http://graph.facebook.com/" + user_id
						+ "/picture?type=square");
				user_photo = BitmapFactory.decodeStream(img_url
						.openConnection().getInputStream());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			imageView_user.setImageBitmap(user_photo);
		}
	}

	class SetUpMatchPhotoTask extends AsyncTask<Void, Void, Void> {

		private Bitmap match_photo;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				URL img_url = new URL("http://graph.facebook.com/" + match_id
						+ "/picture?type=square");
				match_photo = BitmapFactory.decodeStream(img_url
						.openConnection().getInputStream());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			imageView_match.setImageBitmap(match_photo);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case CONNECTION_FAILURE_RESOLUTION_REQUEST:
			switch (resultCode) {
			case Activity.RESULT_OK:
				break;
			}
			break;
		}
	}

	@Override
	protected void onStart() {
		super.onStart();

		mLocationClient.connect();
	}

	@Override
	protected void onStop() {
		if (mLocationClient.isConnected()) {
			mLocationClient.disconnect();
		}

		super.onStop();
	}

	/**
	 * Verify that Google Play services is available before making a request.
	 * 
	 * @return true if Google Play services is available, otherwise false
	 */
	private boolean servicesConnected() {

		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);

		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			// Display an error dialog
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					this, 0);
			if (dialog != null) {
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(dialog);
				errorFragment.show(getSupportFragmentManager(), "");
			}
			return false;
		}
	}

	public static class ErrorDialogFragment extends DialogFragment {
		// Global field to contain the error dialog
		private Dialog mDialog;

		// Default constructor. Sets the dialog field to null
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		// Set the dialog to display
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		// Return a Dialog to the DialogFragment.
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		refresh();
	}

	private void refresh() {
		MovieHelper.getWeekTable(user_id, new APICallBack() {
			@Override
			public void onSuccess(MovieTableModel result) {
				super.onSuccess(result);

				table_people_data = result.getPeople();
				table_density_data = result.getDesity();

				if (table != null)
					table.removeAllViews();
				setUpTable();
			}
		});
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		if (connectionResult.hasResolution()) {
			try {
				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(this,
						CONNECTION_FAILURE_RESOLUTION_REQUEST);
				/*
				 * Thrown if Google Play services canceled the original
				 * PendingIntent
				 */
			} catch (IntentSender.SendIntentException e) {
				// Log the error
				e.printStackTrace();
			}
		} else {
			/*
			 * If no resolution is available, display a dialog to the user with
			 * the error.
			 */
			Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
					connectionResult.getErrorCode(), this,
					CONNECTION_FAILURE_RESOLUTION_REQUEST);
			if (errorDialog != null) {
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				errorFragment.setDialog(errorDialog);
				errorFragment.show(getSupportFragmentManager(), "");
			}
		}
	}

	@Override
	public void onConnected(Bundle arg0) {
		if (servicesConnected()) {
			Location location = mLocationClient.getLastLocation();
			MovieHelper.setLogin(user_name, user_id, location.getLatitude(),
					location.getLongitude(), new APICallBack() {

						@Override
						public void onSuccess(String result) {
							super.onSuccess(result);
						}

						@Override
						public void onFailure(String errorMessage) {
							super.onFailure(errorMessage);
						}
					});
			// MovieHelper.setPreferTime(user_id, 0, 0, location.getLongitude(),
			// location.getLatitude(), new APICallBack() {
			//
			// @Override
			// public void onSuccess(String result) {
			// super.onSuccess(result);
			//
			// Toast.makeText(HallActivity.this, result,
			// Toast.LENGTH_LONG).show();
			// }
			//
			// @Override
			// public void onFailure(String errorMessage) {
			// super.onFailure(errorMessage);
			//
			// Toast.makeText(HallActivity.this, errorMessage,
			// Toast.LENGTH_LONG).show();
			// }
			// });
		}
	}

	@Override
	public void onDisconnected() {

	}
}
