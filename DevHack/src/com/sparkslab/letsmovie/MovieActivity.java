package com.sparkslab.letsmovie;

import java.io.IOException;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MovieActivity extends Activity {

	private int index;
	private String chinese_movie_name;
	private String english_movie_name;
	private String movie_subtitle;
	private String movie_poster_url;
	private String movie_language;
	private String movie_description;
	private String movie_genre;
	private String movie_actors;
	private String movie_length;
	private String theaters;

	private ImageView imageView_poster;
	private TextView textView_chinese_movie_name, textView_english_movie_name,
			textView_movie_language, textView_movie_genre,
			textView_movie_length, textView_content;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_movie);
		getBundle();
		getViews();
		setUpViews();
	}

	private void getBundle() {
		Bundle bundle = getIntent().getExtras();
		index = bundle.getInt("index");
		chinese_movie_name = bundle.getString("chinese_movie_name");
		english_movie_name = bundle.getString("english_movie_name");
		movie_subtitle = bundle.getString("movie_subtitle");
		movie_poster_url = bundle.getString("movie_poster_url");
		movie_language = bundle.getString("movie_language");
		movie_description = bundle.getString("movie_description");
		movie_genre = bundle.getString("movie_genre");
		movie_actors = bundle.getString("movie_actors");
		movie_length = bundle.getString("movie_length");
		theaters = bundle.getString("theaters");
	}

	private void getViews() {
		imageView_poster = (ImageView) findViewById(R.id.imageView_poster);
		textView_chinese_movie_name = (TextView) findViewById(R.id.textView_chinese_movie_name);
		textView_english_movie_name = (TextView) findViewById(R.id.textView_english_movie_name);
		textView_movie_genre = (TextView) findViewById(R.id.textView_movie_genre);
		textView_movie_length = (TextView) findViewById(R.id.textView_movie_length);
		textView_movie_language = (TextView) findViewById(R.id.textView_movie_language);
		textView_content = (TextView) findViewById(R.id.textView_content);
	}

	private void setUpViews() {
		try {
			imageView_poster.setImageDrawable(Drawable.createFromStream(
					getAssets().open("poster_" + index + ".jpg"), null));
		} catch (IOException e) {
			e.printStackTrace();
		}
		textView_chinese_movie_name.setText(chinese_movie_name);
		textView_english_movie_name.setText(english_movie_name);
		textView_movie_genre.setText(movie_genre);
		textView_movie_length.setText("片長" + movie_length);
		textView_movie_language.setText(movie_language);
		textView_content.setText(movie_description + "\n\n" + movie_actors
				+ "\n\n場次：\n" + theaters);
	}
}
