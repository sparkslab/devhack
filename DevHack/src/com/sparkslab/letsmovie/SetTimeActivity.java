package com.sparkslab.letsmovie;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.sparkslab.lets.callbacks.APICallBack;
import com.sparkslab.letsmovie.helper.MovieHelper;

public class SetTimeActivity extends Activity implements OnClickListener {
	Button confirm, cancel;
	String fbUid;
	int hour, weekday, color;
	double longtitude, latitude;
	TextView textView_title, textView_content;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_time);

		Bundle bundle = this.getIntent().getExtras();
		confirm = (Button) findViewById(R.id.ok);
		cancel = (Button) findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		confirm.setOnClickListener(this);
		fbUid = bundle.getString("uid");
		hour = bundle.getInt("hour");
		weekday = bundle.getInt("weekday");
		longtitude = bundle.getDouble("longtitude");
		latitude = bundle.getDouble("latitude");
		color = bundle.getInt("color");
		findViewById(R.id.root).setBackgroundColor(color);

		Calendar calendar = Calendar.getInstance();
		int delta = weekday - calendar.get(Calendar.DAY_OF_WEEK) + 1;
		calendar.add(Calendar.DAY_OF_WEEK, delta);

		SimpleDateFormat sdf_date = new SimpleDateFormat("MM/dd");

		textView_title = (TextView) findViewById(R.id.textView_title);
		textView_title.setText(String.format("%s  %02d ~ %02d",
				sdf_date.format(new Date(calendar.getTimeInMillis())), hour,
				hour + 2));
		textView_content = (TextView) findViewById(R.id.textView_content);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ok:
			MovieHelper.setPreferTime(fbUid, hour, weekday, longtitude,
					latitude, new APICallBack() {

						@Override
						public void onSuccess(String result) {
							super.onSuccess(result);
							if (result.equals("successful")) {
								// TODO
								// addNotification();

								Intent intent = new Intent(
										SetTimeActivity.this,
										CheckService.class);
								intent.putExtra("uid", fbUid);
								startService(intent);
								
								finish();
							}
						}

						@Override
						public void onFailure(String errorMessage) {
							super.onFailure(errorMessage);
						}
					});
			break;
		}

	}
}
