package com.sparkslab.lets.models;

import java.util.List;

public class MovieModel {
	public String chinese_movie_name;
	public String english_movie_name;
	public String movie_subtitle;
	public String movie_poster_url;
	public String movie_language;
	public String movie_description;
	public String movie_genre;
	public String movie_actors;
	public String movie_length;
	public String movie_rating;
	public List<Theater> theaters;

	public static class Theater {
		public String theater_name;
		public String theater_address;
		public double theater_latitude;
		public double theater_longitude;
		public List<String> theater_times;
	}
}
