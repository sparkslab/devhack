package com.sparkslab.lets.callbacks;

import com.sparkslab.lets.models.CheckModel;
import com.sparkslab.lets.models.MovieTableModel;

public class APICallBack {
	public void onSuccess(String result) {
	}

	public void onSuccess(MovieTableModel result) {
	}

	public void onSuccess(CheckModel result) {
	}

	public void onFailure(String errorMessage) {
	}
}
